//
//  FeatureFeedCollectionViewCell.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 15/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class FeatureFeedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var highlight: UILabel!
    @IBOutlet weak var featureImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
