//
//  FeedCollectionViewCell.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 01/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class FeedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var collection: UICollectionView!
    
    
    override func awakeFromNib() {
        let nib = UINib(nibName: "cellHorizontal", bundle: nil)
        collection.register(nib, forCellWithReuseIdentifier: "horizontalCell")
        
        self.collection.delegate = self
        self.collection.dataSource = self
    }
}

extension FeedCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "horizontalCell", for: indexPath)
        
        return cell
    }
}
