//
//  FeedViewModel.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 27/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import Foundation
import Alamofire

protocol FeedProtocol {
    func loadSeries(onCompletion: @escaping () -> Void, onFail: @escaping (String) -> Void)
    func loadMovies(onCompletion: @escaping () -> Void, onFail: @escaping (String) -> Void)
}

class FeedViewModel: FeedProtocol {
    
    func loadSeries(onCompletion: @escaping () -> Void, onFail: @escaping (String) -> Void) {
        
    }
    
    func loadMovies(onCompletion: @escaping () -> Void, onFail: @escaping (String) -> Void) {
        
    }
    
}
