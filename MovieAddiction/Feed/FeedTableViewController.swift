//
//  FeedTableViewController.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 15/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {

    
    @IBOutlet weak var feedMovieImgView: UIImageView!
    @IBOutlet weak var feedSerieImgView: UIImageView!
    @IBOutlet weak var feedPeopleImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.feedMovieImgView.layer.cornerRadius = 10
        self.feedSerieImgView.layer.cornerRadius = 10
        self.feedPeopleImgView.layer.cornerRadius = 10
        self.feedMovieImgView.clipsToBounds = true
        self.feedSerieImgView.clipsToBounds = true
        self.feedPeopleImgView.clipsToBounds = true
        
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "settings"), for: .normal)
        btn.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        let barbutton = UIBarButtonItem(customView: btn)
        self.navigationItem.rightBarButtonItem = barbutton
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            tableView.deselectRow(at: indexPath, animated: true)
            let storyboard = UIStoryboard(name: "MoviesStoryboard", bundle: nil)
            if let controller = storyboard.instantiateInitialViewController() {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case 1:
            tableView.deselectRow(at: indexPath, animated: true)
            let storyboard = UIStoryboard(name: "MoviesStoryboard", bundle: nil)
            if let controller = storyboard.instantiateInitialViewController() {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case 2:
            tableView.deselectRow(at: indexPath, animated: true)
            let storyboard = UIStoryboard(name: "ActorsStoryboard", bundle: nil)
            if let controller = storyboard.instantiateInitialViewController() {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        default:
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

}
