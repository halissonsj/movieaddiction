//
//  MapAnnotationView.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 07/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit
import MapKit

class MapAnnotationView: MKAnnotationView {

    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup() {
        self.backgroundColor = UIColor.lightGray
        label.textAlignment = .center
        self.addSubview(label)
    }
}
