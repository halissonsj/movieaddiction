//
//  AppDelegate.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 26/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD
import Alamofire
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let dataManager = DataManager()
    let apikey = "e33953847731f1715d2407a927137301"
    let placesApiKey = "AIzaSyDjmrAAoLZb0E93IpeT6RlGzqBsect809Y"
    var token: String = ""
    var expireAt: Date?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.configureColor()
        SVProgressHUD.setDefaultStyle(.dark)
        self.configureGenres()
        self.loadHTTPCookies()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
//        self.saveHTTPCookies()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
//        self.loadHTTPCookies()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveHTTPCookies()
        dataManager.saveContext()
    }

}
extension AppDelegate {
    
    func configureColor() {
        
//        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = Util.hexStringToUIColor(hex: "#C13F10")
        UINavigationBar.appearance().tintColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    /**
     Função que, se o usuario não tiver os genres, carrega os mesmos.
     */
    func configureGenres() {
        
        if dataManager.validateGenres() {
            print("GENRES EM SEGURANÇA")
        } else {
            Alamofire.request("https://api.themoviedb.org/3/genre/movie/list?api_key=\(AppDelegate().apikey)&language=pt-BR").responseJSON { (response) in
                if let response = response.result.value {
                    if let data = JSON(response).dictionaryObject {
                        if let arrayGenres = data["genres"] as? [[String:Any]]{
                            for genre in arrayGenres {
                                if let id = genre["id"] as? Int {
                                    if let name = genre["name"] as? String {
                                        self.dataManager.saveGenre(id: id, name: name)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
extension AppDelegate {
    
    func loadHTTPCookies() {
        var cookieDictionary = UserDefaults.standard.value(forKey: "cookieArray") as? [AnyHashable]
        
        for i in 0..<(cookieDictionary?.count ?? 0) {
            let cookieDictionary1 = UserDefaults.standard.value(forKey: cookieDictionary?[i] as? String ?? "") as? [AnyHashable : Any]
            var cookie: HTTPCookie?
            if let aDictionary1 = cookieDictionary1 as? [HTTPCookiePropertyKey : Any] {
                cookie = HTTPCookie(properties: aDictionary1)
            }
            if let aCookie = cookie {
                HTTPCookieStorage.shared.setCookie(aCookie)
            }
        }
    }
    
    func saveHTTPCookies() {
        var cookieArray: [AnyHashable] = []
        for cookie: HTTPCookie? in HTTPCookieStorage.shared.cookies ?? [] {
            cookieArray.append(cookie?.name ?? "")
            var cookieProperties: [AnyHashable : Any] = [:]
            if let nome = cookie?.name {
                cookieProperties[cookie?.name] = nome
                cookieProperties[cookie?.value] = cookie?.value
                cookieProperties[cookie?.domain] = cookie?.domain
                cookieProperties[cookie?.path] = cookie?.path
                if let version = cookie?.version as NSNumber? {
                    cookieProperties[cookie?.version] = Int(truncating: version)
                }
                cookieProperties[cookie?.expiresDate] = Date().addingTimeInterval(2629743)
                
                UserDefaults.standard.setValue(cookieProperties, forKey: nome)
                UserDefaults.standard.synchronize()
            }
        }
        
        UserDefaults.standard.setValue(cookieArray, forKey: "cookieArray")
        UserDefaults.standard.synchronize()
    }
}

