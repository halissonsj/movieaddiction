//
//  ActorDetailViewModel.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 07/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import Foundation
import Alamofire

protocol ActorDetailProtocol {
    
    func loadData(onSucces: @escaping () -> Void, onFail: @escaping (String) -> Void)
    
}
class ActorDetailViewModel: ActorDetailProtocol {
    
    func loadData(onSucces: @escaping () -> Void, onFail: @escaping (String) -> Void) {
        
        Alamofire.request("https://api.themoviedb.org/3/person/popular?api_key=e33953847731f1715d2407a927137301&language=pt-BR&page=1")
        
    }
    
    
    
}
