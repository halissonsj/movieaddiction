//
//  DetailTableViewController.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 26/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var similarMoviesCollection: UICollectionView!
    
    var viewModel: DetailMovieProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        self.viewModel?.loadDetails(onSuccess: {
            self.similarMoviesCollection.delegate = self
            self.similarMoviesCollection.dataSource = self
            if let det = self.viewModel?.details {
                if let imgUrl = det.backdropPath {
                    Util.loadImage(with: imgUrl, onSuccess: { (image) in
                        self.image.image = image
                        self.image.contentMode = .scaleAspectFit
                    })
                }
            }
        }, onFail: { (MSG) in
            print(MSG)
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

}

extension DetailTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = self.viewModel {
            return viewModel.similarMovies?.movies?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarMovieCell", for: indexPath) as? SimilarMoviesCollectionViewCell else {
        return UICollectionViewCell()
        }
        
        if let movies = self.viewModel?.similarMovies {
            Util.loadImage(with: movies.movies?[indexPath.row].posterPath ?? "") { (image) in
                cell.movieImageView.image = image
            }
        }
        return cell
    }
}
