//
//  SimilarMoviesCollectionViewCell.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 09/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit

class SimilarMoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
}
