//
//  FeedTableViewController.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 26/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit
import SVProgressHUD

class MoviesFeedTableViewController: UITableViewController {

    var viewModel: FeedMoviesProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Filmes"

        self.viewModel = FeedMoviesViewModel()
        
        self.tableView.tableFooterView = UIView()
        
        self.load()
    }
    
    func load() {
        SVProgressHUD.show()
        self.viewModel?.loadData(onSuccess: {
            SVProgressHUD.dismiss()
            
            self.tableView.reloadData()
        }, onFail: { (msg) in
            SVProgressHUD.dismiss()
            
            let alert = UIAlertController(title: "Atenção", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as? DetailTableViewController
                let viewModel = DetailMovieViewModel(idMovie: self.viewModel?.arrayMovies[indexPath.row].id ?? 0)
                controller?.viewModel = viewModel
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let viewModel = self.viewModel {
            if viewModel.arrayMovies.count > 0 {
                return 8
            }
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let viewModel = self.viewModel {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as? FeedMoviesTableViewCell {
                
                cell.setupCell(with: viewModel.arrayMovies[indexPath.row])
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "detailsSegue", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
