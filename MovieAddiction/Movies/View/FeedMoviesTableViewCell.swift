//
//  FeedTableViewCell.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 26/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit
import Alamofire

class FeedMoviesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var movieImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var voteLbl: UILabel!
    @IBOutlet weak var starImgView: UIImageView!
    
    let viewModel = FeedMoviesViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with movie: Movie) {
        self.titleLbl.text = movie.title
        
        self.starImgView.image = UIImage(named: "star")
        if let vote = movie.voteAverage {
            self.voteLbl.text = "\(vote)"
            self.starImgView.isHidden = false
        } else {
            self.starImgView.isHidden = true
        }
        
        self.setupGenre(with: movie)
        if let url = movie.backdropPath {
            Util.loadImage(with: url) { (image) in
                self.movieImgView.image = image
            }
        }
    }
    
    func setupGenre(with movie: Movie) {
        if let genres = movie.genreIDS {
            var string = ""
            var i = 0
            for genre in genres {
                if let genreName = self.viewModel.getGenreById(genre) {
                    if genres.count - 1 != i {
                        string.append(genreName + ", ")
                        i = i + 1
                    } else {
                        string.append(genreName)
                    }
                }
            }
            self.genreLbl.text = string
        }
    }
}
