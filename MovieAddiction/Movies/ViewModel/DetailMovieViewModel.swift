//
//  DetailMovieViewModel.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 09/09/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import Foundation
import Alamofire

protocol DetailMovieProtocol {
    var details: DetailsMovie? {get set}
    var similarMovies: Movies? {get set}
    func loadDetails(onSuccess:@escaping () -> Void, onFail:@escaping (String) -> Void)
}

class DetailMovieViewModel: DetailMovieProtocol {
    
    var idMovie = 0
    var details: DetailsMovie?
    var similarMovies: Movies?
    
    init(idMovie: Int) {
        self.idMovie = idMovie
    }
    
    func loadDetails(onSuccess:@escaping () -> Void, onFail:@escaping (String) -> Void) {
        Alamofire.request("https://api.themoviedb.org/3/movie/\(self.idMovie)?api_key=\(AppDelegate().apikey)&language=pt-BR").responseJSON { (response) in
            if let data = response.data {
                let details = try? newJSONDecoder().decode(DetailsMovie.self, from: data)
                self.details = details
                self.loadSimilarMovies(onSuccess: {
                    onSuccess()
                    return
                }, onFail: { (msg) in
                    onFail(msg)
                    return
                })
            } else {
                onFail("Erro")
            }
        }
    }
    
    private func loadSimilarMovies(onSuccess:@escaping () -> Void, onFail:@escaping (String) -> Void) {
        Alamofire.request("https://api.themoviedb.org/3/movie/\(self.idMovie)/similar?api_key=\(AppDelegate().apikey)&language=pt-BR&page=1").responseJSON { (response) in
            if let data = response.data {
                let movies = try? newJSONDecoder().decode(Movies.self, from: data)
                self.similarMovies = movies
                onSuccess()
            } else {
                onFail("Error when load Similar")
            }
        }
    }
}
