//
//  FeedViewModel.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 26/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import Foundation
import Alamofire

protocol FeedMoviesProtocol {
    var arrayMovies: [Movie] {get set}
    
    func loadData(onSuccess: @escaping () -> Void, onFail: @escaping (String) -> Void)
    func getGenreById(_ id: Int) -> String?
}

class FeedMoviesViewModel: FeedMoviesProtocol {
    
    var arrayMovies: [Movie] = []
    private let dataManager = DataManager()
    
    func loadData(onSuccess: @escaping () -> Void, onFail: @escaping (String) -> Void) {
        
        let requisicao = "https://api.themoviedb.org/3/movie/now_playing?api_key=\(AppDelegate().apikey)&language=pt-BR&page=1"
        
        Alamofire.request(requisicao).responseMovies { (response) in
            
            print(requisicao)
            
            if let value = response.result.value {
                if let movies = value.movies {
                    self.arrayMovies = movies
                    onSuccess()
                    return
                }
            }
            onFail("Não foi possível carregar o Feed")
        }
    }
    
    func getGenreById(_ id: Int) -> String? {
        
        if let name = self.dataManager.getGenreById(id) {
            return name
        }
        return nil
    }
    
}
