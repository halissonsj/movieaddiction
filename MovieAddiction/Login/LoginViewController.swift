//
//  ViewController.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 26/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit
import SVProgressHUD
import LocalAuthentication

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginTxtfld: UITextField!
    @IBOutlet weak var senhaTxtFld: UITextField!
    @IBOutlet weak var idAuth: UISwitch!
    
    var viewModel: LoginProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        idAuth.addTarget(self, action: #selector(switchIsChanged(toogle:)), for: UIControl.Event.valueChanged)
        
        self.viewModel = LoginViewModel()
        
        //for test
        self.loginTxtfld.text = "halissonsj"
        self.senhaTxtFld.text = "135ha1001"
        
        if UserDefaults.standard.bool(forKey: "idAuth") {
            self.idAuth.setOn(true, animated: true)
        } else {
            self.idAuth.setOn(false, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        
        if UserDefaults.standard.bool(forKey: "idAuth") {
            self.loginWithID()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.loginTxtfld.resignFirstResponder()
        self.senhaTxtFld.resignFirstResponder()
    }
    
    @IBAction func Logar(_ sender: Any) {
        guard let login = loginTxtfld.text, let senha = senhaTxtFld.text else {
            return
        }
        self.login(login: login, senha: senha)
    }
    
    @IBAction func cadastrar(_ sender: Any) {
        self.chamaWebViewCadastro()
    }
    
    @objc func switchIsChanged(toogle: UISwitch) {
        if toogle.isOn {
            let alert = UIAlertController(title: "Ativar touch id", message: "Deseja ativar o touch id?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action) in
                UserDefaults.standard.set(true, forKey: "idAuth")
                let alertConfirm = UIAlertController(title: "Aviso", message: "Para confirmar touch id, efetue login.", preferredStyle: .alert)
                alertConfirm.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertConfirm, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Não", style: .default, handler: { (action) in
                self.idAuth.setOn(false, animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Desativar touch id", message: "Deseja desativar o touch id?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action) in
                let alertConfirm = UIAlertController(title: "Aviso", message: "Para confirmar a desativação do touch id, efetue login.", preferredStyle: .alert)
                alertConfirm.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertConfirm, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Não", style: .default, handler: { (action) in
                self.idAuth.setOn(true, animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loginWithID() {
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            let reason = "Autenticar usando o touch id"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { (success, error) in
                
                if success {
                    if let pass = UserDefaults.standard.value(forKey: "pwd") as? String, let user = UserDefaults.standard.value(forKey: "usr") as? String {
                        self.login(login: user, senha: pass)
                    }
                }
            }
        }
    }
    
    func setupToken() {
        SVProgressHUD.show()
        self.viewModel?.createToken(onSuccess: {_ in 
            SVProgressHUD.dismiss()
        }, onFail: { (msg) in
            SVProgressHUD.dismiss()
            
            let alert = UIAlertController(title: "Atenção", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Tentar Novamente", style: .default, handler: { (action) in
                self.setupToken()
            }))
            
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func login(login: String, senha: String) {
        if !login.isEmpty {
            if !senha.isEmpty {
                if Util.isConnectedToNetwork() {
                    SVProgressHUD.show()
                    self.viewModel?.newSession(login: login, senha: senha, onSuccess: {
                        SVProgressHUD.dismiss()
                        if self.idAuth.isOn {
                            UserDefaults.standard.set(true, forKey: "idAuth")
                            UserDefaults.standard.set(login, forKey: "usr")
                            UserDefaults.standard.set(senha, forKey: "pwd")
                        } else {
                            UserDefaults.standard.set(false, forKey: "idAuth")
                            UserDefaults.standard.removeObject(forKey: "usr")
                            UserDefaults.standard.removeObject(forKey: "pwd")
                        }
                        self.performSegue(withIdentifier: "loguedSegue", sender: self)
                    }, onFail: { (msg) in
                        SVProgressHUD.dismiss()
                        let alert = UIAlertController(title: "Atenção", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            } else {
                let alert = UIAlertController(title: "Atenção", message: "O campo senha é obrigatório", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Atenção", message: "O campo login é obrigatório", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func chamaWebViewCadastro() {
        if Util.isConnectedToNetwork() {
            let wVController = WebViewViewController()
            wVController.link = "https://www.themoviedb.org/account/signup"
            self.navigationController?.pushViewController(wVController, animated: true)
        }
    }
    
    func visitant() {
        
    }
}

