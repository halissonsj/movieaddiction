//
//  LoginViewModel.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 28/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import Foundation
import Alamofire

protocol LoginProtocol {
    
    func createToken(onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void)
    func newSession( login: String, senha: String, onSuccess: @escaping () -> Void, onFail: @escaping (String) -> Void)
}

class LoginViewModel: LoginProtocol {
    
    /**
     Para logar na api, ela precisa de um request token de autorizacao, assim eu o crio neste método
     */
    func createToken(onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        let url = "https://api.themoviedb.org/3/authentication/token/new?api_key=\(AppDelegate().apikey)"
        
        Alamofire.request(url).responseJSON { (response) in
            if let result = response.data {
                if let token = try? newJSONDecoder().decode(Token.self, from: result), let success = token.success {
                    if success {
                        if let requestToken = token.requestToken, let expiresAt = token.expiresAt {
                            AppDelegate().token = requestToken
                            if let date = expiresAt.toDate(dateFormat: "yyyy-MM-dd hh:mm:ss") {
                                AppDelegate().expireAt = date
                                onSuccess(requestToken)
                                return
                            }
                            onSuccess(requestToken)
                            return
                        }
                    }
                    onFail("erro fail")
                }
                onFail("erro parse")
            }
        }
    }
    
    func newSession( login: String, senha: String, onSuccess: @escaping () -> Void, onFail: @escaping (String) -> Void) {
        
        let url = "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=\(AppDelegate().apikey)"
        
        self.createToken(onSuccess: { (token) in
//            let token = AppDelegate().token
            let parametros: [String: Any] = ["username": login,
                                             "password": senha,
                                             "request_token": token]
            print(url)
            print("parametros: \(parametros)")
            Alamofire.request(url, method: .post, parameters: parametros).responseJSON { (response) in
                if let result = response.data {
                    if let login = try? newJSONDecoder().decode(Login.self, from: result) {
                        if let success = login.success {
                            if success {
                                if let requestToken = login.requestToken, let expiresAt = login.expiresAt {
                                    AppDelegate().token = requestToken
                                    if let date = expiresAt.toDate(dateFormat: "yyyy-MM-dd hh:mm:ss") {
                                        AppDelegate().expireAt = date
                                        onSuccess()
                                        return
                                    }
                                    onSuccess()
                                    return
                                }
                            }
                            onFail("Error")
                        } else {
                            if let errorLogin = try? newJSONDecoder().decode(LoginError.self, from: result) {
                                switch errorLogin.statusCode {
                                case 30:
                                    onFail("Nome de usuário e / ou senha inválidos: você não forneceu um login válido.")
                                    return
                                case 17:
                                    onFail("Sessão Negada.")
                                    return
                                default:
                                    print(errorLogin)
                                    onFail("Erro no auth")
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }, onFail: {(msg) in
            
        })
    }
}


