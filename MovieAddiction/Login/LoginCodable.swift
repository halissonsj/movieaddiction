//
//  LoginCodable.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 28/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//
// To parse the JSON, add this file to your project and do:
//
//   let token = try? newJSONDecoder().decode(Token.self, from: jsonData)

import Foundation

struct Token: Codable {
    let success: Bool?
    let expiresAt, requestToken: String?
    
    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}

struct Login: Codable {
    let success: Bool?
    let expiresAt, requestToken: String?
    
    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}

struct LoginError: Codable {
    let statusCode: Int?
    let statusMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
