//
//  String.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 28/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import Foundation

extension String {
    
    func toDate( dateFormat format  : String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        if let date = dateFormatter.date(from: self) {
            return date
        }
        return nil
    }
    
}
