//
//  WebViewViewController.swift
//  MovieAddiction
//
//  Created by Halisson da Silva Jesus on 30/08/18.
//  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView?
    var link: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        if let url = URL(string: link) {
            self.webView?.load(URLRequest(url: url))
            webView?.allowsBackForwardNavigationGestures = true
        }
    }
    
    override func loadView() {
        webView = WKWebView()
        webView?.navigationDelegate = self
        self.view = webView
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        dismiss(animated: true, completion: nil)
    }

}
