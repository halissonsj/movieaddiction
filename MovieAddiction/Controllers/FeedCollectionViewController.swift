////
////  FeedCollectionViewController.swift
////  MovieAddiction
////
////  Created by Halisson da Silva Jesus on 01/09/18.
////  Copyright © 2018 Halisson da Silva Jesus. All rights reserved.
////
//
//import UIKit
//
//private let reuseIdentifier = "Cell"
//
//class FeedCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        let nib = UINib(nibName: "header", bundle: nil)
//        self.collectionView.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerMenu")
//        
////        collectionLayout.sectionHeadersPinToVisibleBounds
//        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.sectionHeadersPinToVisibleBounds = true
//            
//        }
//        
//    }
//
//    // MARK: UICollectionViewDataSource
//
//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 2
//    }
//
//
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of items
//        if section == 0 {
//            return 1
//        }
//        return 4
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        
//        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "horizontalCell", for: indexPath) as? FeedCollectionViewCell
//        
//        return cell!
//    }
//    
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        
//        guard kind == UICollectionView.elementKindSectionHeader else {
//            return UICollectionReusableView()
//        }
//        return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerMenu", for: IndexPath(item: 0, section: 1))
//    }
//    
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        if section < 1 {
//            return CGSize(width: 0, height: 0)
//        }
//        return CGSize(width: collectionView.bounds.size.width, height: 50)
//    }
//    
//
//}
